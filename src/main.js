import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueRouter)

import ListComponent from './components/ListComponent.vue'
import DetailComponent from './components/DetailComponent.vue'
import ListCategory from './components/ListCategory.vue'
import ListType from './components/ListType.vue'

Vue.config.productionTip = false

const routes = [
  { path: '/', component: ListComponent },
  { path: '/detail/:id', component: DetailComponent },
  { path: '/category/:id/elements', component: ListCategory,name:'list-category' },
  { path: '/type/:id/elements', component: ListType,name:'list-type' },
]

const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})

new Vue({  
  router ,
  render: h => h(App),
}).$mount('#app')
